<?php

namespace App\Http\Controllers;

use App\Items;
use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Items::all();

        return view('index', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'image' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'name' => 'required|string|min:1',
            'description' => 'required|string|min:1',
            'quantity' => 'required|integer|min:1',
            'price' => 'required|integer|min:1',
            'category' => 'required|integer'
        ]);

        $file_path = $request->file('image')->store('public');
        $image = [
            '1' => str_replace('/', '', str_replace('public', '', $file_path))
        ];

        $item = new Items;
        $item->category_id = $request->category;
        $item->images = json_encode($image);
        $item->item_name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->quantity = $request->quantity;
        $item->weight = 0;
        
        if($item->save()) {
            return redirect()->back()->with([
                'success' => 1,
                'message' => 'Barang berhasil ditambahkan',
            ]);
        } else {
            return redirect()->back()->with([
                'success' => 0,
                'message' => 'Barang gagal ditambahkan',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function show($id, Items $items)
    {
        $item = $items->findOrFail($id);
        $cart = Cart::where([['user_id', Auth::id()], ['item_id', $id]])->get();

        $random_items = $items->all()->random(4);
        foreach($random_items as $items) {
            $items->price = 'Rp' . number_format($items->price, 0, '.', '.');
        }
        $random_items;

        return view('items.detail', [
            'item' => $item,
            'random_items' => json_encode($random_items),
            'cart' => $cart,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Items $items)
    {
        return $items->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items $items)
    {
        $validator = $request->validate([
            'name' => 'required|string|min:1',
            'description' => 'required|string|min:1',
            'quantity' => 'required|integer|min:1',
            'price' => 'required|integer|min:1',
            'category' => 'required|integer'
        ]);

        $item = $items->find($request->item_id);
        $item->category_id = $request->category;
        $item->item_name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->quantity = $request->quantity;
        
        if($item->save()) {
            return redirect()->back()->with([
                'success' => 1,
                'message' => 'Barang berhasil diubah',
            ]);
        } else {
            return redirect()->back()->with([
                'success' => 0,
                'message' => 'Barang gagal diubah',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Items $items)
    {
        if($items->find($id)->delete()) {
            return response()->json([
                'success' => 1,
            ]);
        } else {
            return response()->json([
                'success' => 0,
            ]);
        }
    }
}
