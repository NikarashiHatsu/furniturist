<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Items;
use App\Delivery;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $cart_items = Cart::where('user_id', $user_id)->get();

        $items_collection = collect($cart_items);
        $total = 0;
        
        foreach($items_collection as $item) {
            $item->details = Items::findOrFail($item->item_id);
            $item->details->raw_price = $item->details->price;
            $item->details->price = 'Rp' . number_format($item->details->price, 0, '.', '.');

            $total += $item->details->raw_price * $item->quantity;
        }

        $items_collection->total_price = $total;

        return view('cart.index', [
            'cart_items' => $items_collection
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $cart = new Cart;
        $cart->user_id = Auth::id();
        $cart->item_id = $id;
        $cart->quantity = $request->quantity;

        if($cart->save()) {
            return response()->json([
                'success' => 1
            ]);
        } else {
            return response()->json([
                'success' => 0
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Cart $cart)
    {
        $item = Cart::find($id);
        $item->quantity = $request->quantity;
        
        if($item->save()) {
            return response()->json([
                'success' => 1
            ]);
        } else {
            return response()->json([
                'success' => 0
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $item = Cart::find($id);
        
        if($item->delete()) {
            return response()->json([
                'success' => 1
            ]);
        } else {
            return response()->json([
                'success' => 0
            ]);
        }
    }
}
