<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Delivery;
use App\Items;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $cart_items = Cart::where('user_id', '=', $user_id)->get();

        $items_collection = collect($cart_items);
        $total = 0;
        
        foreach($items_collection as $item) {
            $item->details = Items::find($item->item_id);
            $item->details->raw_price = $item->details->price;
            $item->details->price = 'Rp' . number_format($item->details->price, 0, '.', '.');

            $total += $item->details->raw_price * $item->quantity;
        }
        
        $items_collection->total_price = $total;
        
        return view('cart.checkout', [
            'items_collection' => $items_collection
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'payment_check' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'alamat' => 'required|string',
            'kota' => 'required|string',
            'provinsi' => 'required|string',
            'kode_pos' => 'required|string'
        ]);

        $delivery = new Delivery;
        $delivery->user_id = Auth::id();
        $delivery->items = json_encode(Cart::where('user_id', Auth::id())->get());
        $delivery->payment_check_dir = $request->file('payment_check')->store('payment');
        $delivery->deliver_into = $request->alamat . '. ' . $request->kota . ', ' . $request->provinsi . ', ' . $request->kode_pos;
        $delivery->delivery_code = Str::random(12);
        $delivery->delivered = 0;
        $delivery->approved = 0;
        $delivery->save();
        
        $items_in_cart = Cart::where('user_id', Auth::id())->delete();

        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Delivery $delivery)
    {
        $delivery = $delivery->find($id);
        $delivery->delivered = 1;

        if($delivery->save()) {
            return response()->json([
                'success' => 1
            ]);
        } else {
            return response()->json([
                'error' => 0
            ]);
        }
    }

    /**
     * Approval
     */
    public function approve($id, Delivery $delivery)
    {
        $delivery = $delivery->find($id);
        $delivery->approved = 1;

        for($i = 0; $i < count(json_decode($delivery->items)); $i++) {
            $item = json_decode($delivery->items)[$i];
            
            $raw_item = Items::find($item->item_id);
            $raw_item->quantity = $raw_item->quantity - $item->quantity;
            $raw_item->update();
        }

        if($delivery->save()) {
            return response()->json([
                'success' => 1
            ]);
        } else {
            return response()->json([
                'success' => 0,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Delivery $delivery)
    {
        $delivery = $delivery->find($id);

        if($delivery->delete()) {
            return response()->json([
                'success' => 1,
            ]);
        } else {
            return response()->json([
                'success' => 0,
            ]);
        }
    }
}
