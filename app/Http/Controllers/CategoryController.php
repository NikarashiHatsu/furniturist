<?php

namespace App\Http\Controllers;

use App\Category;
use App\Items;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = collect(Category::all());

        foreach($categories as $category) {
            $category->items = Items::all();
        }

        return view('category.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;

        if($category->save()) {
            return redirect()->back()->with([
                'success' => 1,
            ]);
        } else {
            return redirect()->back()->with([
                'success' => 0,
                'message' => 'Gagal menambahkan kategori. Kontak developer untuk masalah ini.'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Category $category)
    {
        return $category->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category = $category->find($request->id);
        $category->name = $request->name;

        if($category->save()) {
            return redirect()->back()->with([
                'success' => 1,
                'message' => 'Kategori berhasil diubah.',
            ]);
        } else {
            return redirect()->back()->with([
                'success' => 0,
                'message' => 'Kategori gagal diubah. Kontak developer untuk masalah ini.',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Category $category)
    {
        if($category->find($id)->delete()) {
            return response()->json([
                'success' => 1,
                'message' => 'Kategori berhasil dihapus.',
            ]);
        } else {
            return response()->json([
                'success' => 0,
                'message' => 'Kategori gagal dihapus. Kontak developer untuk masalah ini.',
            ]);
        }
    }
}
