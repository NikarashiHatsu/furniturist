<?php

namespace App\Http\Controllers;

use App\User;
use App\Items;
use App\Category;
use App\Delivery;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->role == 'user') {
            $deliveries = Delivery::where([['user_id', Auth::id()], ['delivered', 0]])->get();

            return view('user.index', [
                'deliveries' => $deliveries
            ]);
        } else {
            $deliveries = Delivery::where([['user_id', Auth::id()], ['delivered', 0]])->get();
            
            // TODO: Ganti ke admin.index
            return view('user.index', [
                'deliveries' => $deliveries
            ]);
        }
    }

    /**
     * Update the user data
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {
        $user = User::find(Auth::id());
        $user->name = $request->username;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->province = $request->province;
        $user->postal_code = $request->postal_code;
        $user->email = $request->email;
        $user->save();

        if($request->from_ajax) {
            return response()->json([
                'success' => 1
            ]);
        }

        return redirect(route('user.profile'));
    }

    /**
     * Update the user's password
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function password_update(Request $request)
    {
        $request->validate([
            'old_password' => 'required|min:8',
            'new_password' => 'required|min:8|confirmed'
        ]);
        
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->new_password);
        $user->save();

        Auth::logout();

        return redirect('/')->with('status', 'Password telah diperbarui.');
    }

    /**
     * Check if the user is an admin
     */
    private function check_admin() {
        $user = User::find(Auth::id());

        if($user->role != 'admin') {
            abort(404);
        }
    }

    /**
     * Show the Admin module
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admin() {
        $this->check_admin();

        $categories = Category::all();
        $orders = Delivery::where('approved', '=', 0)->get();
        $items = Items::all();
        
        return view('user.admin', [
            'categories' => $categories,
            'input_categories' => $categories,
            'input_categories_edit' => $categories,
            'items' => $items,
            'orders' => $orders,
        ]);
    }

    public function payment($id) {
        return Storage::get('payment/' . $id);
    }

}
