<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/pages/index', function() {
//     return view('homepage');
// });

Auth::routes([
    'verify' => false,
    'reset' => false
]);

Route::get('/home', function() {
    return redirect('/');
});

Route::get('/', 'ItemsController@index')->name('items');
Route::get('/item/{id}', 'ItemsController@show')->name('items.show');

Route::group(['middleware' => 'auth'], function() {
    
    // ITEM MODULE
    Route::group(['prefix' => 'items'], function() {
        Route::post('/', 'ItemsController@store')->name('items.store');
        Route::get('/edit/{id}', 'ItemsController@edit')->name('items.edit');
        Route::put('/update', 'ItemsController@update')->name('items.update');
        Route::delete('/destroy/{id}', 'ItemsController@destroy')->name('items.destroy');
    });

    // CART MODULE
    Route::group(['prefix' => 'cart'], function() {
        Route::get('/', 'CartController@index')->name('cart');
        Route::post('/add/{id}', 'CartController@store')->name('cart.store');
        Route::put('/update/{id}', 'CartController@update')->name('cart.update');
        Route::delete('/remove/{id}', 'CartController@destroy')->name('cart.destroy');
    });

    // CHECKOUT MODULE
    Route::group(['prefix' => 'checkout'], function() {
        Route::get('/', 'DeliveryController@index')->name('delivery.index');
        Route::post('/', 'DeliveryController@store')->name('delivery.store');
        Route::put('/update/{id}', 'DeliveryController@update')->name('delivery.update');
        Route::put('/approve/{id}', 'DeliveryController@approve')->name('delivery.approve');
        Route::delete('/destroy/{id}', 'DeliveryController@destroy')->name('delivery.destroy');
    });

    // CATEGORY MODULE
    Route::group(['prefix' => 'category'], function() {
        Route::get('/', 'CategoryController@index')->name('category.index');
        Route::post('/', 'CategoryController@store')->name('category.store');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('category.edit');
        Route::put('/update', 'CategoryController@update')->name('category.update');
        Route::delete('/destroy/{id}', 'CategoryController@destroy')->name('category.destroy');
    });

    // PROFILE MODULE
    Route::group(['prefix' => 'profile'], function() {
        Route::get('/', 'HomeController@index')->name('user.profile');
        Route::put('/', 'HomeController@update')->name('user.update');
        Route::put('/password_update', 'HomeController@password_update')->name('user.password_update');
    });

    // ADMIN MODULE
    Route::group(['prefix' => 'admin'], function() {
        Route::get('/', 'HomeController@admin')->name('admin.index');
    });

    // PAYMENT
    Route::get('/payment/{id}', 'HomeController@payment');

});
