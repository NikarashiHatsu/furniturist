<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            [
                'category_id' => 1,
                'images' => '{ "1": "prod1.jpg"}',
                'item_name' => 'Wall Clock',
                'description' =>  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.',
                'price' => 100000,
                'quantity' => 100,
                'weight' => 1.5
            ],
            [
                'category_id' => 1,
                'images' => '{ "1": "prod2.jpg"}',
                'item_name' => 'Elegant Pendant',
                'description' =>  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.',
                'price' => 250000,
                'quantity' => 100,
                'weight' => 2
            ],
            [
                'category_id' => 1,
                'images' => '{ "1": "prod3.jpg"}',
                'item_name' => 'Cactus',
                'description' =>  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.',
                'price' => 50000,
                'quantity' => 100,
                'weight' => 0.75
            ],
            [
                'category_id' => 1,
                'images' => '{ "1": "prod4.jpg"}',
                'item_name' => 'Shelf',
                'description' =>  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.',
                'price' => 200000,
                'quantity' => 100,
                'weight' => 3
            ],
            [
                'category_id' => 1,
                'images' => '{ "1": "prod5.jpg"}',
                'item_name' => 'Table',
                'description' =>  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.',
                'price' => 750000,
                'quantity' => 100,
                'weight' => 7
            ],
            [
                'category_id' => 1,
                'images' => '{ "1": "prod6.jpg"}',
                'item_name' => 'Modern Chair',
                'description' =>  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.',
                'price' => 300000,
                'quantity' => 100,
                'weight' => 4.5
            ],
        ]);
        
        DB::table('carts')->insert([
            [
                'item_id' => 1,
                'user_id' => 1,
                'quantity' => 1,
            ],
            [
                'item_id' => 2,
                'user_id' => 1,
                'quantity' => 1,
            ],
            [
                'item_id' => 3,
                'user_id' => 1,
                'quantity' => 1,
            ],
        ]);
    }
}
