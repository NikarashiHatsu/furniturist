<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Aghits Nidallah',
                'password' => Hash::make('12344321'),
                'email' => 'shiroyuki@domain.id',
                'role' =>  'admin',
            ],
            [
                'name' => 'Muh. Sholeh',
                'password' => Hash::make('12344321'),
                'email' => 'raffeigh@domain.id',
                'role' =>  'user',
            ],
            [
                'name' => 'M. Arief Firmansyah',
                'password' => Hash::make('12344321'),
                'email' => 'kkaren@domain.id',
                'role' =>  'user',
            ],
            [
                'name' => 'Taufik Imam Pramono',
                'password' => Hash::make('12344321'),
                'email' => 'aikyuu@domain.id',
                'role' =>  'user',
            ],
            [
                'name' => 'Aula Nur Rizal A.',
                'password' => Hash::make('12344321'),
                'email' => 'naverlyn@domain.id',
                'role' =>  'user',
            ],
        ]);
    }
}
