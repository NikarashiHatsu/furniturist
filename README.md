## How to Install
1.  Download this repository's source code via Gitlab web or using `git clone git@gitlab.com:NikarashiHatsu/furniturist.git`
2.  Run `composer install --optimize-autoloader`.
3.  Run `npm install`.
4.  Run `npm run dev`.
5.  Run `php artisan serve`.
6.  Open your browser and locate into address that artisan server has made.

**It's recommended to use Git. Because just in case I pushed a new update, you shouldn't be doing all of the things above.**

## Todo List
### Front End
1.  Home Page (**Done**)
2.  Item Detail Page (**Done**)
3.  Cart Page (**Done**)
4.  Authentication Page (**Done**)
5.  Checkout Page (**Done**)
6.  Category Page (**Done**)
7.  Admin Page (**Done**)
8.  User Profile Page (**Done**)

### Functionality
1.  Homepage show items (**done**)
2.  Item detail page show item's detail (**done**)
3.  Item detail page add to cart (**done**)
4.  Item detail page remove from cart (**done**)
5.  Item detail page change item quantity (**done**)
6.  Cart page delete item from cart (**done**)
7.  Cart page update item quantity (**done**)
8.  Category show items (**done**)
9.  Checkout page's checkout (**done**)
10. After checkout, move all user's Cart in to Delivery using JSON format and delete all the user's Cart (**done**)
11. User profile update address (**done**)
12. User profile update password (**done**)
13. Admin page add item (**done**)
14. Admin page edit item (**done**)
15. Admin page delete item (**done**)
16. Admin page add category (**done**)
17. Admin page edit category (**done**)
18. Admin page delete category (**done**)
19. Admin page notification for delivery approval (**done**)

### Future Planning
1.  [Functionality] Admin could ban user (**abandoned**)
2.  [Functionality] Admin could delete user (**abandoned**)

### Need Improvement
1.  Alert sukses ketika checkout (**done**)
2.  Alert konfirmasi perubahan ketika edit user (**done**)
3.  Alert sukses / gagal ketika edit user (**done**)
4.  Tambah fitur upload gambar bukti pembayaran di halaman Checkout (**done**)
5.  Tambah fitur pembatalan penghapusan kategori saat ada item yang masih terdaftar pada kategori tersebut (**done**)

### Bugs Found
1.  Update cart menggunakan index item id (**done**)
2.  Jumlah barang berkurang sebelum approval admin (**done**)