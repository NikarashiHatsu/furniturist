@extends('layouts.app')

@section('content')

    @php
        $image = json_decode($item->images, TRUE);
        $image_link = asset('/storage/' . $image[1]);
        $in_cart = false;
        $price = 'Rp' . number_format($item->price, 0, '.', '.');

        if(count($cart) > 0) {
            $in_cart = true;
        }
    @endphp
    
    <item-detail
        item-id="{{ $item->id }}"
        item-image="{{ $image_link }}"
        item-name="{{ $item->item_name }}"
        item-price="{{ $price }}"
        item-description="{{ $item->description }}"
        item-quantity="{{ $item->quantity }}"
        in-cart-id="{{ (isset($cart->first()->id) ? $cart->first()->id : 0) }}"
        in-cart-quantity="{{ (isset($cart->first()->quantity) ? $cart->first()->quantity : 1) }}"
        in-cart="{{ $in_cart }}"></item-detail>

    <item-related
        random-items="{{ $random_items }}"
        asset-link="{{ asset('/storage/') }}"
        item-link="{{ url('/item/') }}"></item-related>
        
@endsection