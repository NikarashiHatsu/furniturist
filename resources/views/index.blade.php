@extends( 'layouts.app' )

@section( 'content' )

    <!-- ALERT -->
    @if(session('status'))
        <div class="row product-case pt-5 px-5">
            <div class="col">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif
    <!-- /ALERT -->

    <!-- START::ITEM DISPLAY -->
    <div class="row product-case p-5">

        @forelse($items as $item)
        
            @php
                $image = json_decode($item->images, TRUE);
                $image = asset('/storage/' . $image[1]);

                $price = 'Rp' . number_format($item->price, 0, '.', '.');

                $link = url('/item/' . $item->id);
            @endphp
            
            <div class="col-4 mb-4">
                <item-display
                    item-link="{{ $link }}"
                    item-image="{{ $image }}"
                    item-name="{{ $item->item_name }}"
                    price="{{ $price }}"
                    description="{{ $item->description }}"></item-display>
            </div>
            
        @empty
        
        @endforelse
    </div>
    <!-- END::ITEM DISPLAY -->
    
@endsection