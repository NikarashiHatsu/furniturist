@extends('layouts.app')

@section('content')
<div class="row p-5">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-xs-12 col-sm-12 mb-5">
        <h2 class="mb-4">Profil</h2>
        <div class="card">
            <div class="card-body d-flex align-items-center justify-content-between">
                <div id="user">
                    <i class="fas fa-user mr-4"></i>
                    {{ Auth::user()->name }}
                </div>
                <div id="control">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#editProfile">
                        <i class="fas fa-edit mr-3"></i>
                        Edit Profil
                    </button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#editPassword">
                        <i class="fas fa-lock mr-3"></i>
                        Ubah Password
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12">
        <h2 class="mb-4">Pesanan Anda</h2>

        @forelse($deliveries as $delivery)
            <div class="card mb-3">
                <div class="card-header">
                    ID Pesanan: {{ $delivery->delivery_code }} - 
                    @if($delivery->approved == 1)
                        <div class="badge badge-success">
                            Disetujui
                        </div>
                    @else
                        <div class="badge badge-danger">
                            Belum disetujui
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Harga</th>
                                <th>Kuantitas</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                            @endphp
                            @for($i = 0; $i < count(json_decode($delivery->items)); $i++)
                                @php
                                    $item = json_decode($delivery->items)[$i];
                                    $raw_item = \App\Items::find($item->item_id);
                                    $total += $raw_item->price * $item->quantity;
                                @endphp
                                <tr>
                                    <td>{{ $raw_item->item_name }}</td>
                                    <td>{{ 'Rp' . number_format($raw_item->price, 0, '.', '.') }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ 'Rp' . number_format($raw_item->price * $item->quantity, 0, '.', '.') }}</td>
                                </tr>
                            @endfor
                            <tr>
                                <td colspan='3'><b>Total</b></td>
                                <td>{{ 'Rp' . number_format($total, 0, '.', '.') }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-success" onclick="konfirmasiTransaksi({{ $delivery->id }})"  {{ ($delivery->approved == 0 ? 'disabled' : '') }}>
                        Selesaikan Transaksi
                    </button>
                </div>
            </div>
        @empty
            Belum ada pesanan untuk saat ini.
        @endforelse

    </div>
</div>
<div class="modal fade" tabindex="1" role="dialog" id="editPassword">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Edit Password</h3>
            </div>
            <div class="modal-body">
                <form action="{{ route('user.password_update') }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="old_password">Password Lama</label>
                        <input type="password" name="old_password" class="form-control" minlength="8" required />
                    </div>
                    <div class="form-group">
                        <label for="new_password">Password Baru</label>
                        <input type="password" name="new_password" class="form-control" minlength="8" required />
                    </div>
                    <div class="form-group">
                        <label for="new_password_confirmation">Konfirmasi Password Baru</label>
                        <input type="password" name="new_password_confirmation" class="form-control" minelength="8" required />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            Ubah Password
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="1" role="dialog" id="editProfile">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Edit Informasi</h3>
            </div>
            <div class="modal-body">
                <form action="{{ route('user.update') }}" method="post" id="updateProfileForm" onsubmit="updateProfile(event)">
                    @method('PUT')
                    @csrf
                    <input type="hidden" name="from_ajax" value="1" />
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" value="{{ Auth::user()->name }}" required />
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat (Blok Rt / Rw, Desa, Kecamatan)</label>
                        <input type="text" name="address" class="form-control" value="{{ Auth::user()->address }}" />
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <input type="text" name="city" class="form-control" value="{{ Auth::user()->city }}" />
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <input type="text" name="province" class="form-control" value="{{ Auth::user()->province }}" />
                    </div>
                    <div class="form-group">
                        <label for="email">Kode Pos</label>
                        <input type="number" name="postal_code" class="form-control" value="{{ Auth::user()->postal_code }}" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}" required />
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success">
                            Perbarui
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    function updateProfile(event) {
        event.preventDefault();

        swal.fire({
            title: 'Simpan perubahan?',
            text: 'Apakah Anda ingin menyimpan perubahan ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
                    },
                    url: $("#updateProfileForm").attr('action'),
                    data: $("#updateProfileForm").serialize(),
                    method: 'put',
                    success: function(res) {
                        if(res['success'] == 1) {
                            swal.fire({
                                title: 'Sukses!',
                                text: 'Profil Anda telah diubah.',
                                icon: 'success',
                            }).then((result) => {
                                if(result.value) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal.fire(
                                'Error',
                                'Ada kesalahan pada saat mengubah detail profil. Kontak developer untuk masalah ini.',
                                'error'
                            );
                        }
                    },
                    error: function(res) {
                        swal.fire(
                            'Error',
                            'Terjadi kesalahan pada server. Kontak developer untuk masalah ini.',
                            'error'
                        );
                    }
                });
            }
        });
    }
    
    function konfirmasiTransaksi(idDelivery) {
        swal.fire({
            title: 'Konfirmasi Transaksi?',
            text: 'Apakah paket Anda sudah sampai?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sudah',
            cancelButtonText: 'Belum',
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
                    },
                    url: '/checkout/update/' + idDelivery,
                    method: 'put',
                    success: function(res) {
                        if(res['success'] == 1) {
                            swal.fire({
                                title: 'Terima kasih!',
                                text: 'Terima kasih atas kepercayaan Anda untuk membeli dari toko kami.',
                                icon: 'success'
                            }).then((result) => {
                                if(result.value) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal.fire(
                                'Error',
                                'Ada kesalahan pada saat memproses penyelesaian transaksi. Kontak developer untuk masalah ini.',
                                'error'
                            );
                        }
                    },
                    error: function(res) {
                        swal.fire(
                            'Error',
                            'Terjadi kesalahan pada server. Kontak developer untuk masalah ini.',
                            'error'
                        );
                    }
                });
            }
        });
    }
</script>
@endsection