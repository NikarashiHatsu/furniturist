@extends('layouts.app')

@section('content')
<div class="row p-5">
    <div class="col">

        @if(session('success') !== NULL)
            @if(session('success') == 1)
                <div class="alert alert-success mb-4">
                    {{ session('message') }}
                </div>
            @else
                <div class="alert alert-danger mb-4">
                    {{ session('message') }}
                </div>
            @endif
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ups!</strong> Ada yang salah dengan input Anda.
                <ul class="pl-3 mt-3">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <h2>Admin Page</h2>
    </div>
</div>
<div class="row px-5">

    <!-- ITEMS -->
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-copy mr-3"></i>
                Barang-barang
            </div>
            <ul class="list-group list-group-flush">
                @forelse($items as $item)
                    <li class="list-group-item">
                        <div class="row d-flex justify-content-between">
                            <div class="col-auto d-flex align-items-center">
                                <img src="{{ asset('/storage') . '/' . json_decode($item->images, 1)[1] }}" style="width: 100px;" class="shadow-sm">
                            </div>
                            <div class="col-7 py-1">
                                <h5>{{ $item->item_name }} - {{ $item->quantity }}pcs</h5>
                                <p class="mb-0">{{ $item->description }}</p>
                            </div>
                            <div class="col-auto d-flex align-items-center">
                                <button class="btn btn-outline-success mr-2" onclick="itemEdit( {{ $item->id }} )">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button class="btn btn-outline-danger" onclick="itemDelete( {{ $item->id }} )">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </li>
                @empty
                    <li class="list-group-item">
                        Belum ada barang
                    </li>
                @endforelse
            </ul>
            <div class="card-footer d-flex flex-row-reverse" data-toggle="modal" data-target="#addModalItem">
                <button class="btn btn-outline-primary btn-sm">
                    <i class="fas fa-plus mr-3"></i>
                    Tambah Barang
                </button>
            </div>
        </div>
    </div>

    <!-- CATEGORIES & NOTIFICATIONS-->
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-tags mr-3"></i>
                Kategori-kategori
            </div>
            <ul class="list-group list-group-flush">
                @forelse( $categories as $category )
                    <li class="list-group-item d-flex align-items-center justify-content-between">
                        {{ $category->name  }}
                        <div id="buttonWrapper">
                            <button class="btn btn-outline-success mr-2" onclick="categoryEdit( {{ $category->id }} )">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button class="btn btn-outline-danger" onclick="categoryDelete( {{ $category->id }} )">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </li>
                @empty
                    <li class="list-group-item d-flex align-items-center justify-content-between">
                        Belum ada kategori
                    </li>
                @endforelse
            </ul>
            <div class="card-footer d-flex flex-row-reverse">
                <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#addModalCategory">
                    <i class="fas fa-plus mr-3"></i>
                    Tambah kategori
                </button>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-bell mr-3"></i>
                Notifikasi
            </div>
            <ul class="list-group list-group-flush">
                @forelse($orders as $order)
                    <li class="list-group-item">
                        <h5 class="mt-2 mb-3">List order:</h5>
                        <table class="table table-sm table-bordered table-hover mb-4">
                            <thead>
                                <th>Nama Barang</th>
                                <th>Kuantitas</th>
                            </thead>
                            <tbody>
                                @for($i = 0; $i < count(json_decode($order->items)); $i++)
                                    @php
                                        $item = json_decode($order->items)[$i];
                                    @endphp
                                    <tr>
                                        <td>{{ (\App\Items::find($item->id))->item_name }}</td>
                                        <td>{{ $item->quantity }}</td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                        <p><b>Kirim ke</b>: {{ $order->deliver_into }}</p>
                        <h5 class="mb-3">Bukti Pembayaran:</h5>
                        <img src="{{ $order->payment_check_dir }}" class="w-100 shadow-sm mb-3">
                        <div id="approvalButtons" class="d-flex justify-content-between">
                            <button class="btn btn-success w-100 mr-3" onclick="deliveryApprove( {{ $order->id }} )">
                                <i class="fas fa-check mr-3"></i>
                                Setujui
                            </button>
                            <button class="btn btn-danger w-100" onclick="deliveryDecline( {{ $order->id }} )">
                                <i class="fas fa-times mr-3"></i>
                                Batalkan
                            </button>
                        </div>
                    </li>
                @empty
                    <li class="list-group-item">
                        Belum ada pesanan yang masuk
                    </li>
                @endforelse
            </ul>
        </div>
    </div>

</div>

<div class="modal fade" id="addModalCategory">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Tambah Kategori
            </div>
            <div class="modal-body">
                <form action="{{ route('category.store') }}" method="post" id="formCategoryAdd">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama Kategori</label>
                        <input type="text" name="name" class="form-control" minlength="1" required />
                    </div>
                </form>
            </div>
            <div class="modal-footer d-flex flex-row-reverse">
                <button class="btn btn-success" type="submit" onclick="$('#formCategoryAdd').submit()">
                    <i class="fas fa-plus mr-3"></i>
                    Tambah Kategori
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModalCategory">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Edit Modal
            </div>
            <div class="modal-body">
                <form action="{{ route('category.update') }}" method="post" id="formCategoryUpdate">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Nama Kategori</label>
                        <input type="text" name="name" class="form-control" id="namaKategori" minlength="1" required />
                        <input type="hidden" name="id" id="idKategori" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" onclick="categoryUpdate(event)">
                    <i class="fas fa-save"></i>
                    Simpan perubahan
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModalItem">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Tambah Item
            </div>
            <div class="modal-body">
                <form action="{{ route('items.store') }}" method="post" id="formAddItem" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Unggah foto</label>
                            <input type="file" class="custom-file-input" name="image" id="customFile" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Barang</label>
                        <input type="text" class="form-control" name="name" minlength="1" required />
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea name="description" cols="30" rows="10" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="quantity">Kuantitas</label>
                        <input type="number" class="form-control" name="quantity" minlength="1" required />
                    </div>
                    <div class="form-group">
                        <label for="price">Harga</label>
                        <input type="number" class="form-control" name="price" minlength="1" required />
                    </div>
                    <div class="form-group">
                        <label for="category">Kategori</label>
                        <select name="category" class="form-control" required>
                            @if($input_categories->count() > 0)
                                <option value="" disabled hidden selected>Pilih Kategori</option>
                                @foreach($input_categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" onclick="$('#formAddItem').submit()">
                    <i class="fas fa-plus mr-3"></i>
                    Tambah Barang
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModalItem">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Edit Barang
            </div>
            <div class="modal-body">
                <form action="{{ route('items.update') }}" method="post" id="formItemUpdate">
                    @method('PUT')
                    @csrf
                    <input type="hidden" name="item_id" id="itemId" />
                    <div class="form-group">
                        <label for="name">Nama Barang</label>
                        <input type="text" class="form-control" name="name" minlength="1" id="namaBarang" required />
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea name="description" cols="30" rows="10" class="form-control" id="deskripsiBarang" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="quantity">Kuantitas</label>
                        <input type="number" class="form-control" name="quantity" minlength="1" id="kuantitasBarang" required />
                    </div>
                    <div class="form-group">
                        <label for="price">Harga</label>
                        <input type="number" class="form-control" name="price" minlength="1" id="hargaBarang" required />
                    </div>
                    <div class="form-group">
                        <label for="category">Kategori</label>
                        <select name="category" class="form-control" required>
                            @if($input_categories_edit->count() > 0)
                                <option value="" disabled hidden selected>Pilih Kategori</option>
                                @foreach($input_categories_edit as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" onclick="itemUpdate(event)">
                    <i class="fas fa-save mr-3"></i>
                    Simpan perubahan
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    // MODUL KATEGORI
    function categoryEdit(categoryId) {
        $.ajax({
            url: '/category/edit/' + categoryId,
            success: function(res) {
                $("input#namaKategori").val(res['name']);
                $("input#idKategori").val(res['id']);
                $("#editModalCategory").modal('show');
            },
            error: function(res) {
                swal.fire({
                    title: 'Gagal!',
                    text: 'Ada kesalahan saat menerima data kategori',
                    icon: 'error'
                });
            }
        });
    }

    function categoryUpdate(event) {
        event.preventDefault();

        swal.fire({
            title: 'Edit Kategori?',
            text: 'Apakah Anda ingin mengubah kategori ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if(result.value) {
                $("#formCategoryUpdate").submit();
            }
        });
    }

    function categoryDelete(categoryId) {
        swal.fire({
            title: 'Hapus Kategori?',
            text: 'Apakah Anda yakin ingin menghapus kategori ini?',
            icon: 'warning',
            showCancelButton: true,
            dangerMode: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    url: '/category/destroy/' + categoryId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'delete',
                    success: function(res) {
                        swal.fire({
                            title: 'Sukses!',
                            text: 'Kategori telah dihapus.',
                            icon: 'success',
                        }).then((result) => {
                            if(result.value) {
                                window.location.reload();
                            }
                        });
                    },
                    error: function(res) {
                        swal.fire({
                            title: 'Gagal!',
                            text: 'Ada kesalahan pada server. Kontak developer untuk masalah ini.',
                            icon: 'error'
                        });
                    }
                });
            }
        });
    }

    // MODUL ITEM
    function itemEdit(itemId) {
        $.ajax({
            url: '/items/edit/' + itemId,
            success: function(res) {
                $("input#itemId").val(res['id']);
                $("input#namaBarang").val(res['item_name']);
                $("textarea#deskripsiBarang").html(res['description']);
                $("input#kuantitasBarang").val(res['quantity']);
                $("input#hargaBarang").val(res['price']);
                $("#editModalItem").modal('show');
            },
            error: function() {
                swal.fire({
                    title: 'Error!',
                    text: 'Ada kesalahan saat menerima data dari server. Kontak developer untuk masalah ini.',
                    icon: 'error'
                });
            }
        });
    }

    function itemUpdate(event) {
        event.preventDefault();

        swal.fire({
            title: 'Edit barang?',
            text: 'Apakah Anda ingin mengubah barang ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if(result.value) {
                $("#formItemUpdate").submit();
            }
        });
    }

    function itemDelete(itemId) {
        swal.fire({
            title: 'Hapus barang?',
            text: 'Apakah Anda yakin ingin menghapus barang ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'danger',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    url: '/items/destroy/' + itemId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'delete',
                    success: function(res) {
                        if(res['success'] == 1) {
                            swal.fire({
                                title: 'Berhasil!',
                                text: 'Barang berhasil dihapus!',
                                icon: 'success',
                            }).then((result) => {
                                if(result.value) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal.fire({
                                title: 'Gagal!',
                                text: 'Ada kesalahan pada server. Kontak developer untuk masalah ini.',
                                icon: 'error',
                            });
                        }
                    },
                    error: function(res) {
                        swal.fire({
                            title: 'Gagal!',
                            text: 'Barang gagal dihapus. Kontak developer untuk masalah ini.',
                            icon: 'error',
                        });
                    }
                });
            }
        });
    }

    // APPROVAL
    function deliveryApprove(deliveryId) {
        swal.fire({
            title: 'Setujui?',
            text: 'Setujui pemesanan ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    url: '/checkout/approve/' + deliveryId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'put',
                    success: function(res) {
                        if(res['success'] == 1) {
                            swal.fire({
                                title: 'Berhasil',
                                text: 'Anda telah menyetujui pesanan ini.',
                                icon: 'success',
                            }).then((result) => {
                                if(result.value) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal.fire({
                                title: 'Gagal',
                                text: 'Server tidak bisa memproses persetujuan Anda. Kontak developer untuk masalah ini.',
                                icon: 'error',
                            });
                        }
                    },
                    error: function(res) {
                        swal.fire({
                            title: 'Error',
                            text: 'Ada masalah pada server. Kontak developer untuk masalah ini',
                            icon: 'error',
                        });
                    }
                })
            }
        });
    }

    function deliveryDecline(deliveryId) {
        swal.fire({
            title: 'Tolak?',
            text: 'Tolak pesanan ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    url: '/checkout/destroy/' + deliveryId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'delete',
                    success: function(res) {
                        if(res['success'] == 1) {
                            swal.fire({
                                title: 'Berhasil',
                                text: 'Pesanan telah dihapus.',
                                icon: 'success',
                            }).then((result) => {
                                if(result.value) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal.fire({
                                title: 'Gagal',
                                text: 'Server tidak bisa memproses penghapusan pesanan Anda. Kontak developer untuk masalah ini.',
                                icon: 'error',
                            });
                        }
                    },
                    error: function(res) {
                        swal.fire({
                            title: 'Error',
                            text: 'Ada masalah pada server. Kontak developer untuk masalah ini',
                            icon: 'error',
                        });
                    }
                });
            }
        });
    }
</script>
@endsection