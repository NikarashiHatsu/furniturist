@extends('layouts.app')

@section('content')
<div class="row py-5">
    <div class="col">
        <h2 class="mb-4">
            Daftar Kategori
        </h2>
        @forelse($categories as $category)
            @foreach($category->items as $item)
                @php
                    $item->price = 'Rp' . number_format($item->price, 0, '.', '.');
                @endphp
            @endforeach
            <item-related
                title="{{ $category->name }}"
                random-items="{{ $category->items }}"
                asset-link="{{ asset('/storage/') }}"
                item-link="{{ url('/item/') }}"></item-related>
        @empty
        @endforelse
        </div>
    </div>
</div>
@endsection