@extends('layouts.app')

@section('content')

    <div class="row p-5">
        <div class="col-sm-12">
            <h2 class="mb-5">Keranjang Anda</h2>
            @php
                $show = 0;
            @endphp
            @forelse($cart_items as $item)
                @php
                    $show = 1;
                @endphp
                <cart-control
                    class="mb-4"
                    asset-link="{{ asset('/storage/') }}"
                    object="{{ $item }}"></cart-control>
            @empty
                {{ __('Belum ada barang yang Anda masukkan dalam keranjang') }}
            @endforelse
        </div>
    </div>
    @if($show == 1)
    <div class="row px-5">
        <div class="col-sm-8">
            <h2 class="mb-5">Total Keranjang</h2>
        </div>
        <div class="col-sm-4 text-right">
            <p class="mb-3">{{ __('Rp') . number_format($cart_items->total_price, 0, '.', '.') }}</p>
            <a href="/checkout" class="btn btn-secondary my-3">
                Checkout
            </a>
        </div>
    </div>
    @endif

@endsection