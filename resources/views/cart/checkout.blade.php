@extends('layouts.app')

@section('content')

@if($items_collection->count() > 0)
    <div class="row py-5 mb-4">
        <div class="col">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Ups!</strong> Ada yang salah dengan input Anda.
                    <ul class="pl-3 mt-3">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        
            <h2>Checkout</h2>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-6">
            <h4 class="mb-4">Kelengkapan Identitas</h4>
            <form method="post" action="{{ route('delivery.store') }}" id="checkoutForm" enctype="multipart/form-data">
                @csrf
                <div class="row mb-3">
                    <div class="col">
                        <label for="name">Nama</label>
                        <input type="text" name="name" class="form-control" value="{{ Auth::user()->name ? Auth::user()->name : '' }}" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="alamat">Alamat (Blok, Desa, Kecamatan)</label>
                        <input type="text" name="alamat" class="form-control" value="{{ Auth::user()->address ? Auth::user()->address : '' }}" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="kota">Kota</label>
                        <input type="text" name="kota" class="form-control" value="{{ Auth::user()->city ? Auth::user()->city : '' }}" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="provinsi">Provinsi</label>
                        <input type="text" name="provinsi" class="form-control" value="{{ Auth::user()->province ? Auth::user()->province : '' }}" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="kode_pos">Kode pos</label>
                        <input type="text" name="kode_pos" class="form-control" value="{{ Auth::user()->postal_code ? Auth::user()->postal_code : '' }}" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" value="{{ Auth::user()->email ? Auth::user()->email : '' }}" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Unggah foto bukti pembayaran</label>
                            <input type="file" class="custom-file-input" name="payment_check" id="customFile" required />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button class="btn btn-success" type="submit" onclick="checkout(event)">
                            Konfirmasi
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-6">
            <h4 class="mb-4">Orderan Anda</h4>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Nama Item</th>
                        <th>Quantitas</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($items_collection as $item)
                        <tr>
                            <td>{{ $item->details->item_name }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td class="text-right">{{ __('Rp') . number_format($item->details->raw_price * $item->quantity, 0, '.', '.') }}</td>
                        </tr>
                    @empty
                        {{ __('Kok bisa? Harusnya lo gak boleh kesini sebelum ada barang :/') }}
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2">Total</th>
                        <th class="text-right">{{ __('Rp') . number_format($items_collection->total_price, 0, '.', '.') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@else
    <div class="row p-5">
        <h3>Belum ada barang di keranjang Anda</h3>
    </div>
@endif

@endsection()

@section('script')
<script type="text/javascript">
    function checkout(event) {
        event.preventDefault();
        
        swal.fire({
            title: 'Checkout?',
            text: 'Anda yakin ingin mengkonfirmasi checkout Anda?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
        }).then((result) => {
            if(result.value) {
                $("#checkoutForm").submit();
            }
        });
    }
</script>
@endsection